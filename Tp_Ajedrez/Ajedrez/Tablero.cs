﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ajedrez
{
    public class Tablero
    {
        Pieza[,] tableroAjedrez = new Pieza[8, 8];

        public Tablero()
        {
            RellenarCasillasVacias();
            RellenarConPiezasReales();
        }

        public Pieza[,] TableroAjedrez
        {
            get
            {
                return tableroAjedrez;
            }
        }

        public void RellenarCasillasVacias()
        {
            for(int i = 0; i < 8; i++)
            {
                for(int j = 0; j < 8; j++)
                {
                    tableroAjedrez[i, j] = new CasillaVacia();
                }
            }
        }
        public void RellenarConPiezasReales()
        {
            for (int i = 0; i < 8; i++)
            {
                if(i % 2 == 0)
                {
                    tableroAjedrez[1, i] = new DamaNegro();
                    
                }
                if (i % 2 == 1)
                {
                    tableroAjedrez[6, i] = new DamaBlanco();
                }
            }
        }

        public void Imprimir()
        {
            for(int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Console.Write("|");
                    Console.Write(tableroAjedrez[i,j].LetraPieza);
                }
                Console.WriteLine("|");
            }
            Console.WriteLine("----------------");
        }

        public bool MoverPiezaBlancaDerecha(int x, int y)
        {
            if (IsOnLimite(x, y) == false) 
            { 
                DamaBlanco aux;
                aux = (DamaBlanco)tableroAjedrez[x, y];
                tableroAjedrez[x, y] = new CasillaVacia();
                x--;
                y++;
                tableroAjedrez[x, y] = aux;
                return true;
            }
            return false;
        }

        public bool MoverPiezaBlancaIzquierda(int x, int y)
        {
            if(IsOnLimite(x,y) == false)
            {
                DamaBlanco aux;
                aux = (DamaBlanco)tableroAjedrez[x, y];
                tableroAjedrez[x, y] = new CasillaVacia();
                x--;
                y--;
                tableroAjedrez[x, y] = aux;
                return true;
            }
            return false;
        }

        public bool MoverPiezaNegraDerecha(int x, int y)
        {
            if (IsOnLimite(x, y) == false)
            {
                DamaNegro aux;
                aux = (DamaNegro)tableroAjedrez[x, y];
                tableroAjedrez[x, y] = new CasillaVacia();
                x++;
                y--;
                tableroAjedrez[x, y] = aux;
                return true;
            }
            return false;
        }

        public bool MoverPiezaNegraIzquierda(int x, int y)
        {
            if (IsOnLimite(x, y) == false)
            {
                DamaNegro aux;
                aux = (DamaNegro)tableroAjedrez[x, y];
                tableroAjedrez[x, y] = new CasillaVacia();
                x++;
                y++;
                tableroAjedrez[x, y] = aux;
                return true;
            }
            return false;
        }

        public bool IsOnLimite(int x, int y)
        {
            if (x == 0 || x == 7 || y == 0 || y == 7)
            {
                return true;
            }
            else return false;
        }
    }
}
