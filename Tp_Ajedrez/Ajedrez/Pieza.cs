﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ajedrez
{
    public abstract class Pieza
    {
        protected string letraPieza;

        public Pieza()
        {

        }
        public string LetraPieza
        {
            get
            {
                return letraPieza;
            }
        }
    }
}
