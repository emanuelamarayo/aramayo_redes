﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ajedrez
{
    public class DamaBlanco : Pieza
    {
        public DamaBlanco()
        {
            letraPieza = "B";
        }
    }
}
