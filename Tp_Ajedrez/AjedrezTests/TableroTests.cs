﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ajedrez;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ajedrez.Tests
{
    [TestClass()]
    public class TableroTests
    {
        [TestMethod()]
        public void MoverPiezaBlancaDerechaTest()
        {
            Tablero tablero = new Tablero();
            Assert.IsTrue(tablero.MoverPiezaBlancaDerecha(6, 5));
        }

        [TestMethod()]
        public void MoverPiezaBlancaIzquierdaTest()
        {
            Tablero tablero = new Tablero();
            Assert.IsTrue(tablero.MoverPiezaBlancaIzquierda(6, 5));
        }

        [TestMethod()]
        public void MoverPiezaNegraDerechaTest()
        {
            Tablero tablero = new Tablero();
            Assert.IsTrue(tablero.MoverPiezaNegraDerecha(1, 2));
        }

        [TestMethod()]
        public void MoverPiezaNegraDerechaTest1()
        {
            Tablero tablero = new Tablero();
            Assert.IsFalse(tablero.MoverPiezaNegraDerecha(1, 0));
        }

        [TestMethod()]
        public void MoverPiezaNegraIzquierdaTest()
        {
            Tablero tablero = new Tablero();
            Assert.IsTrue(tablero.MoverPiezaNegraIzquierda(1, 2));
        }

        [TestMethod()]
        public void IsOnLimiteTest()
        {
            Tablero tablero = new Tablero();
            Assert.IsTrue(tablero.IsOnLimite(0, 3));
        }

        [TestMethod()]
        public void IsOnLimiteTest1()
        {
            Tablero tablero = new Tablero();
            Assert.IsFalse(tablero.IsOnLimite(1, 6));
        }
    }
}