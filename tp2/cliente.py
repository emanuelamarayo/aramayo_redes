import socket
import sys

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

ip = input('Ingrese la direcion del servidor: ')
puerto = input('ingrese el puerto: ')
puerto = int (puerto)
ipServer = (ip,puerto)

print('Conectando a {} puerto {}'.format(*ipServer))
sock.connect(ipServer)

try:

    message = b'Mensaje random xd'
    print('sending {!r}'.format(message))
    sock.sendall(message)

    amount_received = 0
    amount_expected = len(message)

    while amount_received < amount_expected:
        data = sock.recv(16)
        amount_received += len(data)
        print('received {!r}'.format(data))

finally:
    print('cerrando socket')
    sock.close()
