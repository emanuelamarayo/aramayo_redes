import socket
import sys

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

ipServer = ('127.0.0.1', 8080)
print('Iniciando conexion en {} puerto {}'.format(*ipServer))
sock.bind(ipServer)

sock.listen(1)

while True:

    print('Esperando a conexion :D')
    connection, ipCliente = sock.accept()
    try:
        print('conexion desde', ipCliente)

        while True:
            data = connection.recv(16)
            print('received{!r}'.format(data))
            if data:
                print('sending data back al cliente')
                connection.sendall(data)
            else:
                print('no data desde', ipCliente)
                break

    finally:
        connection.close()
